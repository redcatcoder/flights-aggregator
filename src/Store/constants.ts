export const GET_FLIGHTS  = 'Flights/GetAll'
export const SET_SORTING = 'Flights/Sorting/Set'
export const SET_FILTER = 'Flights/Filter/Set'
