import {IFlightItem} from "interfaces/flight";
import {Action} from 'redux';

export interface IFilters {
    transfers: null | number,
    price: IPrice,
    carrier: string[]
}

export interface IInitialState {
    flights: IFlightItem[],
    sorting: null | string,
    filters: IFilters,
    isLoading: boolean,
}

interface IPrice {
    minSum: number,
    maxSum: number,
}

export interface IAction extends Action<string> {
    type: string,
    sorting?: string,
    filters?: IFilters,
    flights?: Array<IFlightItem>,
}
