import { GET_FLIGHTS, SET_SORTING, SET_FILTER } from "Store/constants";
import {IAction, IInitialState} from "./types";


const initialState: IInitialState = {
    flights: [],
    sorting: null,
    isLoading: false,
    filters: {
        transfers: null,
        price: {
            minSum: 0,
            maxSum: 9999999999,
        },
        carrier: []
    },
}

export const FlightsReducer = (state = initialState, action: IAction) => {
    switch (action.type) {
        case SET_SORTING:
            return {...state, sorting: action.sorting}
        case GET_FLIGHTS:
            return {...state, flights: [...action.flights]}
        case SET_FILTER:
            return {...state, filters: {...action.filters}}
        default:
            return {...state}
    }
}
