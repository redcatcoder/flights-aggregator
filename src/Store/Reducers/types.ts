import { IInitialState } from "./FlightsReducers/types";

export interface IReducers {
    flights: IInitialState
}
