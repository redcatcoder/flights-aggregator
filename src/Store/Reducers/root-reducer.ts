import { combineReducers } from "redux";
import { FlightsReducer } from "./FlightsReducers/flights-reducer";

export default combineReducers({
    flights: FlightsReducer,
})
