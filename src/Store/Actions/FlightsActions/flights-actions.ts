import { Dispatch } from "redux";
import { IFilters } from "Store/Reducers/FlightsReducers/types";
import { GET_FLIGHTS, SET_FILTER, SET_SORTING } from "Store/constants";
import { FLIGHT_URL } from "Config";
import { Flights } from "./types";

const getFlightsAction = (flights: Flights) => {
    return {
        type: GET_FLIGHTS,
        flights
    }
}

const setFiltersAction = (filters: IFilters) => {
    return {
        type: SET_FILTER,
        filters
    }
}

const setSortingAction = (sorting: string) => {
    return {
        type: SET_SORTING,
        sorting
    }
}

export const getFlights = () => {
    return async (dispatch: Dispatch) => {
        const response = await fetch(FLIGHT_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })

        if (response.ok) {
            const data = await response.json();
            dispatch(getFlightsAction(data.result.flights))
        }
    }
}

export const setSorting = (sorting: string) => {
    return (dispatch: Dispatch) => {
        dispatch(setSortingAction(sorting))
    }
}

export const setFilter = (filters: IFilters) => {
    return (dispatch: Dispatch) => {
        dispatch(setFiltersAction(filters))
    }
}
