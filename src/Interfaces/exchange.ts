interface IExchangeItem {
    amount: string,
    currency: string,
    currencyCode: string
}

interface IExchange {
    [key: string]: IExchangeItem | boolean
}

export default IExchange;
