import { IBasicItem } from "./basic";

interface ICarrier extends IBasicItem {
    airlineCode: string,
    caption: string,
    uid: string
}

export default ICarrier;
