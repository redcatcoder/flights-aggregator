import { IFlightItem } from "./flight";

export interface IBasicItem {
    uid: string,
    caption: string,
}

export interface IBasicPrice {
    amount: string,
    currency?: string,
    currencyCode: string
}

export interface IAppState {
    flights: {
        sorting?: string | null,
        flights?: Array<IFlightItem>
    }
}
