interface IServiceDetails {
    fareBasis: IFareBasis,
    freeCabinLuggage: any,
    freeLuggage: IFreeLuggage,
    paidCabinLuggage: any,
    paidLuggage: IPaidLuggage,
    tariffName: string
}

interface IFareBasis {
    [key: string]: string
}

interface IFreeLuggage {
    [key: string]: IFreeLuggageItem
}
interface IPaidLuggage {
    [key: string]: IPaidLuggageItem
}

interface IFreeLuggageItem {
    pieces: number,
    nil: boolean,
    unit: string
}
interface IPaidLuggageItem {
    pieces: number,
    nil: boolean,
    unit: string
}

export default IServiceDetails;
