import { IBasicItem } from "./basic";
import { IServiceDetails } from "./index";

interface IAircraft extends IBasicItem {}
interface IAirline extends IBasicItem {
    airlineCode: string
}
interface IArrivalAirport extends IBasicItem {}
interface IArrivalCity extends IBasicItem {}
interface IClassOfService extends IBasicItem {}
interface IDepartureAirport extends IBasicItem {}
interface IDepartureCity extends IBasicItem {}

interface ISegment {
    aircraft: IAircraft,
    airline: IAirline,
    arrivalAirport: IArrivalAirport,
    arrivalCity: IArrivalCity,
    arrivalDate: string,
    classOfService: IClassOfService,
    classOfServiceCode: string,
    departureAirport: IDepartureAirport,
    departureCity: IDepartureCity,
    departureDate: string,
    flightNumber: string,
    servicesDetails: IServiceDetails,
    starting: boolean,
    stops: number,
    techStopInfos: Array<any>,
    travelDuration: number
}

export default ISegment
