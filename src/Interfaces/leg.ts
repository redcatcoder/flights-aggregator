import { ISegment } from "./index";

interface ILeg {
    duration: number,
    segments: Array<ISegment>
}

export default ILeg;
