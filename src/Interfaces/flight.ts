import IExchange from "./exchange";
import ILeg from "./leg";
import IPrice from "./price";
import ICarrier from "./carrier";
import { IBasicItem } from "./basic";

export interface IFlight {
    airlineAlliance?: IBasicItem,
    carrier: ICarrier,
    exchange: IExchange,
    international: boolean,
    isTripartiteContractDiscountApplied: boolean,
    legs: Array<ILeg>,
    price: IPrice,
}

export interface IFlightItem {
    hasExtendedFare: boolean,
    flightToken: string,
    flight: IFlight
}
