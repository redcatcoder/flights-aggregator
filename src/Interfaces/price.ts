import { IBasicItem, IBasicPrice } from "./basic";

interface IPrice {
    passengerPrices: Array<IPricePassengerPricesItem>,
    rates: IPriceRates,
    total: IBasicPrice,
    totalFeeAndTaxes: IBasicPrice,
}

export interface IPricePassengerPricesItem {
    feeAndTaxes: IBasicPrice,
    passengerCount: number,
    passengerType: IBasicItem,
    singlePassengerTotal: IBasicPrice,
    tariff: IBasicPrice,
    total: IBasicPrice,
}

interface IPriceRates {
    [key: string]: IBasicPrice
}

export default IPrice
