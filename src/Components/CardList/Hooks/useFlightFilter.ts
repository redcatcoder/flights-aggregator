import {useMemo} from "react";
import { IFlightItem, ILeg } from "Interfaces";
import ICarrier from "Interfaces/carrier";
import { IFilters } from "Store/Reducers/FlightsReducers/types";
import {IPricePassengerPricesItem} from "interfaces/price";

const filteredByTransfers = (flights: IFlightItem[], filters: IFilters) =>
    flights.filter((flight) => {
        const legs: ILeg[] = flight?.flight?.legs
        if (legs?.length === 0) {
            return false
        }

        const approvedFlight = legs?.filter(leg => leg?.segments.length - 1 === filters.transfers)

        return approvedFlight?.length >= legs?.length
    })

const filteredByPrice = (flights: IFlightItem[], filters: IFilters) =>
    flights.filter((flight) => {
        const passengerPrices = flight?.flight?.price.passengerPrices
        if (passengerPrices?.length === 0) {
            return false
        }

        const passengerPricesArray = passengerPrices?.filter((passengerPrice: IPricePassengerPricesItem) =>
            passengerPrice?.passengerType?.uid === 'ADULT'
            && parseInt(passengerPrice.singlePassengerTotal.amount) >= filters.price.minSum
            && parseInt(passengerPrice.singlePassengerTotal.amount) <= filters.price.maxSum
        )

        return passengerPricesArray?.length > 0
    })

const filteredByAirline = (flights: IFlightItem[], filters: IFilters) =>
    flights.filter((flight) => {
        const carrier: ICarrier = flight?.flight?.carrier

        return filters.carrier.includes(carrier.caption)
    })

export const useFlightFilter = (flights: IFlightItem[], filters: IFilters) =>
    useMemo(() => {
        let filteredFlight = flights
        if (typeof filters.transfers === 'number') {
            filteredFlight = filteredByTransfers(filteredFlight, filters)
        }

        if (filters.price.minSum || filters.price.maxSum) {
            filteredFlight = filteredByPrice(filteredFlight, filters)
        }

        if (filters.carrier.length > 0) {
            filteredFlight = filteredByAirline(filteredFlight, filters)
        }

        return filteredFlight
    }, [flights, filters])
