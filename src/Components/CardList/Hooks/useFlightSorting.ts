import { useMemo } from "react";
import { IFlightItem, ILeg } from "Interfaces";

const flightsByPriceUp = (flights: IFlightItem[]) => {
    const prepareFlights = [...flights]

    return prepareFlights.sort((current: IFlightItem, next: IFlightItem) => {
        return parseInt(
            current?.flight?.price?.passengerPrices[0]?.singlePassengerTotal?.amount
        ) - parseInt(
            next?.flight?.price?.passengerPrices[0]?.singlePassengerTotal?.amount
        )
    })
}

const flightsByPriceDown = (flights: IFlightItem[]) => {
    const prepareFlights = [...flights]

    return prepareFlights.sort((current: IFlightItem, next: IFlightItem) => {
        return parseInt(
            next?.flight?.price?.passengerPrices[0]?.singlePassengerTotal?.amount
        ) - parseInt(
            current?.flight?.price?.passengerPrices[0]?.singlePassengerTotal?.amount
        )
    })
}

const flightsByTimeInFly = (flights: IFlightItem[]) => {
    const prepareFlights = [...flights]

    return prepareFlights.sort((current: IFlightItem, next: IFlightItem) => {
        const currentDurations = current.flight.legs.map((leg: ILeg) => leg.duration)
        const nextDurations = next.flight.legs.map((leg: ILeg) => leg.duration)

        return currentDurations
                .reduce((previousValue, nextValue) => previousValue + nextValue, 0)
            - nextDurations
                .reduce((previousValue, nextValue) => previousValue + nextValue, 0)
    })
}

export const useFlightSorting = (flights: IFlightItem[], sortingType: string) =>
    useMemo(() => {

        switch (sortingType) {
            case 'priceUp':
                return flightsByPriceUp(flights);
            case 'priceDown':
                return flightsByPriceDown(flights)
            case 'timeInFly':
                return flightsByTimeInFly(flights)
            default:
                return flights;
        }
    }, [flights, sortingType])
