import { IFlightItem } from "interfaces/flight";
import { useMemo } from "react";

export const useShowMore =
    (flights: IFlightItem[], count = 2) =>
        useMemo(() => flights && flights.slice(0, count), [ flights, count ])
