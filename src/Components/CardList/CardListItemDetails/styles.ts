import IStyles from "Config/styles";

export const Styles: IStyles = {
    itemDetailsTransfers: {
        color: 'secondary.main',
        cursor: 'pointer',
        '&:hover': {
            color: 'secondary.dark'
        }
    },
    itemDetailsHighlightText: {
        color: 'primary.main'
    },
    itemDetailsLocationInfo: {
        display: 'flex',
        justifyContent: 'center',
        borderBottom: 'thin solid rgba(0, 0, 0, 0.12)',

    },
    itemDetailsLocationInfoElement: {
        margin: '5px',
    },
    itemDetailsTiming: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '80%',
        margin: '10px auto'
    },
    carrierInfo: {
        padding: '0 10px'
    }
}
