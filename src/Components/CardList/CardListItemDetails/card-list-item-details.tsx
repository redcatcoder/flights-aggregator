import React, { memo } from "react";
import { Box, Divider, Typography } from "@mui/material";
import { ILeg } from "Interfaces";
import { Styles } from './styles';
import DateTimeFormatOptions = Intl.DateTimeFormatOptions;

export const CardListItemDetails = memo(({
    duration,
    segments
}: ILeg) => {
    const transfers = segments.length - 1;
    const firstSegment = segments[0];
    const lastSegment = segments[transfers]
    const { departureCity, departureAirport, departureDate } = firstSegment;
    const { arrivalCity, arrivalAirport, arrivalDate } = lastSegment;

    const transformTimeInFly = ( timeInFly: number ): string => {
        const time = timeInFly / 60;
        let hours = parseInt( time.toFixed(2).split('.')[0] )
        let minutes = parseInt( time.toFixed(2).split('.')[1] )

        if (minutes >= 60) {
            const hoursToAdd = parseInt( (minutes / 60).toString().split('.')[0] );
            hours += hoursToAdd;
            minutes -= (hoursToAdd * 60)
        }

        if (minutes > 0) {
            return `${hours} ч ${minutes} мин`
        }

        return `${hours} ч`
    }

    const prepareDateTime = ( datetime: string ): string => {
        const options: DateTimeFormatOptions = {hour: "2-digit", minute: "2-digit", day: "numeric", month: "short", weekday: "short"};

        return new Date(datetime).toLocaleDateString("ru-RU", options).split(',').reverse().join(' ')
    }

    const flightLocationInfo = (cityCaption: string, airportCaption: string, airportUid: string): JSX.Element => {
        return (
            <>
                <Typography variant="body1">{cityCaption?.concat(', ')}</Typography>
                <Typography variant="body1">{airportCaption?.concat(' ')}</Typography>
                <Typography sx={Styles.itemDetailsHighlightText} variant="body1">({airportUid})</Typography>
            </>
        )
    }

    return (
        <>
            <Box component="span" sx={Styles.itemDetailsLocationInfo}>
                <Box className="departureInfo" component="span" sx={ Styles.itemDetailsLocationInfoElement }>
                    {flightLocationInfo(departureCity?.caption, departureAirport?.caption, departureAirport?.uid)}
                </Box>
                <Typography variant="body1" sx={[
                    Styles.itemDetailsLocationInfoElement,
                    Styles.itemDetailsHighlightText
                ]}>
                    →
                </Typography>
                <Box className="arrivalInfo" component="span" sx={Styles.itemDetailsLocationInfoElement}>
                    {flightLocationInfo(arrivalCity?.caption, arrivalAirport?.caption, arrivalAirport?.uid)}
                </Box>
            </Box>
            <Box sx={Styles.itemDetailsTiming}>
                <Typography className="departureTime">{prepareDateTime(departureDate)} </Typography>
                <Typography className="timeInFly">{transformTimeInFly(duration)} </Typography>
                <Typography className="arrivalTime">{prepareDateTime(arrivalDate)}</Typography>
            </Box>
            <>
                {
                    (transfers > 0)
                        ? <Divider sx={Styles.itemDetailsTransfers} variant="middle">Пересадок: {transfers}</Divider>
                        : <Divider variant="middle"/>
                }
            </>
            <Typography className="carrierInfo" sx={Styles.carrierInfo}>Рейс выполняет: {}</Typography>
        </>
    )
})
