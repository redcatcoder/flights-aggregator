import React, { memo } from "react";
import { Typography } from "@mui/material";
import { IPricePassengerPricesItem } from "interfaces/price";

export const CardListItemPrices = memo(({
    passengerType,
    singlePassengerTotal,
}: IPricePassengerPricesItem) => {
    const {amount, currency} = singlePassengerTotal;

    return (
        <Typography
            variant="h6"
            className="priceBlock-value"
        >
            {passengerType.caption} билет: {amount} {currency}
        </Typography>
    )
})
