import React, { memo, useEffect, useState } from "react";
import { useDispatch, useSelector} from "react-redux"
import { Box, Button, CircularProgress, List } from "@mui/material";
import { getFlights } from "Store/Actions/FlightsActions/flights-actions";
import { CardListItem } from "./CardListItem/card-list-item";
import { IFlightItem } from "Interfaces";
import { useFlightFilter, useFlightSorting, useShowMore } from "./Hooks";
import { IReducers } from "store/Reducers/types";
import { Styles } from './styles'

const CardList = memo(() => {
    const dispatch = useDispatch()
    const [countFlightOnPage, setCountFlightOnPage] = useState(2)

    useEffect(() => {
        dispatch(getFlights())
    }, [])

    const { flights, sorting, filters } = useSelector((state: IReducers) => ({
        flights: state.flights.flights,
        sorting: state.flights.sorting,
        filters: state.flights.filters,
    }))

    const filteredFlights = useFlightFilter(flights, filters);
    const sortedFlights = useFlightSorting(filteredFlights, sorting)
    const flightsOnPage = useShowMore(sortedFlights, countFlightOnPage)

    const handleClick = () =>
        setCountFlightOnPage(countFlightOnPage + 2)

    if (flightsOnPage.length === 0) {
        return (

            <Box sx={Styles.loaderContainer}>
                <CircularProgress />
            </Box>
        )
    }

    return (
        <>
            <List>
                {
                    flightsOnPage &&
                    flightsOnPage.map( (flightItem: IFlightItem) => <CardListItem key={Math.random()} {...flightItem}/>)
                }
            </List>
            {filteredFlights
                && flightsOnPage
                && flightsOnPage.length < filteredFlights.length
                && (
                    <Box width="100%" flex="flex" justifyContent="center">
                        <Button variant="contained" onClick={handleClick}>Показать еще</Button>
                    </Box>
                )}
        </>

    )
})

export default CardList
