import React, {memo} from "react";
import { Box, Button, Divider, ListItem, Typography } from "@mui/material";
import { IFlightItem, ILeg } from "Interfaces"
import { CardListItemDetails } from "Components/CardList/CardListItemDetails";
import { CardListItemPrices } from "Components/CardList/CardListItemPrices";
import { Styles } from "./styles";
import {IPricePassengerPricesItem} from "interfaces/price";

export const CardListItem = memo(({
   hasExtendedFare,
   flightToken,
   flight,
}: IFlightItem) => {
    const { carrier, legs, price } = flight;

    return (
            <ListItem key={flightToken}  sx={Styles.cardListItem}>
                <Box className="listItem-header" sx={Styles.cardListItemHeader}>
                    <Typography variant="h6">{carrier.caption}</Typography>
                    <Box>
                        {price.passengerPrices.map( (passengerPrice: IPricePassengerPricesItem) => <CardListItemPrices key={Math.random()} {...passengerPrice}/> )}
                    </Box>
                </Box>
                <Box className="listItem-body" sx={Styles.cardListItemBody}>
                    {
                        legs.map( (leg: ILeg, index: number) => {
                            if (index === legs.length - 1) {
                                return (<CardListItemDetails key={Math.random()} {...leg}/>)
                            }

                            return (
                                <>
                                    <CardListItemDetails  key={Math.random()} {...leg}/>
                                    <Divider  key={Math.random()} sx={Styles.cardListItemBodyDivider} />
                                </>
                            )
                        })
                    }
                </Box>
                <Button
                    className="listItem-buyButton"
                    sx={Styles.cardListItemButton}
                >
                    Выбрать
                </Button>
            </ListItem>
    )
})
