import IStyles from "Config/styles";

export const Styles: IStyles = {
    cardListItem: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        padding: 0
    },
    cardListItemHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        backgroundColor: 'primary.main',
        color: 'primary.contrastText',
        width: 'inherit',
        padding: 1,
        boxSizing: 'border-box'
    },
    cardListItemBody: {
        width: 'inherit',
    },
    cardListItemButton: {
        backgroundColor: 'secondary.main',
        color: 'secondary.contrastText',
        borderRadius: '0!important',
        width: 'inherit',
        '&:hover': {
            backgroundColor: 'secondary.dark',
        }
    },
    cardListItemBodyDivider: {
        backgroundColor: 'primary.main',
        padding: '1px 0',
        margin: '10px 0'
    }
}
