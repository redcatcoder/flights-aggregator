import { IStyles } from "Config";

export const Styles: IStyles = {
    loaderContainer: {
        display: 'flex',
        justifyContent: "center",
        alignItems: "center",
        height: "100%"
    }
}
