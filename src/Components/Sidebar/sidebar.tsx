import React, { memo } from 'react';
import { Grid } from "@mui/material";
import { SidebarSorting } from "./SidebarSorting";
import { SidebarFilters } from "./SidebarFilters";
import { Styles } from './styles';

export const Sidebar = memo(() => {
    return (
        <Grid container spacing={2} sx={Styles.sidebarContainer}>
            <Grid item xs={6} md={12} sx={Styles.sidebarItem}>
                <SidebarSorting/>
            </Grid>
            <Grid item xs={6} md={12} sx={Styles.sidebarItem}>
                <SidebarFilters/>
            </Grid>
        </Grid>
    )
})
