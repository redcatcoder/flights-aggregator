import React from "react";
import { memo } from "react";
import { Box, Checkbox, FormControlLabel } from "@mui/material";
import { Styles } from "./styles";
import { useCheckboxDataFlights } from './hooks/useCheckboxDataFlights'
import { IFilterName } from "./types";
import { useDispatch, useSelector } from "react-redux";
import { IReducers } from "Store/Reducers/types";
import { setFilter } from "Store/Actions/FlightsActions/flights-actions";

export const FilterName = memo(({
    flights
}: IFilterName) => {
    const dispatch = useDispatch()
    const dataFlightsForCheckbox = useCheckboxDataFlights(flights)
    const filters = useSelector((state: IReducers) => state.flights.filters)

    const handleChange = (event, name) => {
        let prepareCarrier = filters.carrier

        if (event.target.checked) {
            if (!prepareCarrier.includes(name)) {
                prepareCarrier.push(name)
            }
        } else {
            if (prepareCarrier.includes(name)) {
                prepareCarrier.splice(prepareCarrier.indexOf(name), 1);
            }
        }

        const newFilters = {
            ...filters,
            carrier: prepareCarrier
        }

        dispatch(setFilter(newFilters))
    }

    return (
            <Box sx={Styles.box}>
                {Object.keys(dataFlightsForCheckbox).map((key, index) => (
                    <FormControlLabel
                        key={index}
                        control={
                            <Checkbox value={index} />
                        }
                        onChange={(event) => handleChange(event, key)}
                        label={`${key} от ${dataFlightsForCheckbox[key]}`}
                    />
                ))}
            </Box>
    )
})

