import { IFlightItem } from "interfaces/flight";

export interface IFilterName {
    flights: IFlightItem[]
}
