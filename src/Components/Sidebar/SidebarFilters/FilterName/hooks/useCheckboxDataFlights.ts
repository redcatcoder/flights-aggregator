import { IFlightItem } from "interfaces/flight";
import { useMemo } from "react";
import { IResultFilterNameItem } from "components/Sidebar/SidebarFilters/FilterName/hooks/types";

export const useCheckboxDataFlights =
    (flights: IFlightItem[]): IResultFilterNameItem =>
        useMemo(() => {
            if (flights && flights.length === 0) {
                return null
            }

            return flights.reduce((acc, flight) => {
                const name = flight?.flight.carrier.caption
                const flightsByName = flights.filter(flight => flight?.flight.carrier.caption === name)

                const prices = flightsByName.reduce((acc: number[], flight) => {
                    const passengerPrices = flight?.flight?.price?.passengerPrices

                    if (!passengerPrices) {
                        return acc
                    }

                    passengerPrices.forEach(passengerPrice =>
                        acc.push(parseInt(passengerPrice.singlePassengerTotal.amount))
                    )

                    return acc
                }, [])

                const minPrice = Math.min(...prices)

               return {...acc, [name]: minPrice}
            }, {})
        }, [flights])
