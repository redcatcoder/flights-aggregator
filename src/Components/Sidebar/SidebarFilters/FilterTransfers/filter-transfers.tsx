import React, { useMemo, memo } from "react";
import { useDispatch, useSelector } from "react-redux";

import {
    Checkbox,
    FormControlLabel,
    FormGroup
} from "@mui/material";

import { IFlightItem, ILeg } from "Interfaces";

import { setFilter } from "Store/Actions/FlightsActions/flights-actions";
import {IReducers} from "store/Reducers/types";

const FilterTransfers = memo( () => {

    const dispatch = useDispatch()

    const { flights, filters } = useSelector((state: IReducers) => ({
        flights: state.flights.flights,
        filters: state.flights.filters,
    }))

    const maxTransfers = useMemo( () => Math.max(...flights.map((flightItem: IFlightItem) => {
            return Math.max(...flightItem.flight.legs.map((leg: ILeg) => leg.segments.length - 1))
        })
    ), [flights])

    const onCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newTransfers = (event.target.checked)
            ? parseInt(event.target.value)
            : null;

        dispatch( setFilter({...filters, ...{transfers: newTransfers}}) )
    }

    let checkBoxes = [];

    for (let i = 0; i <= maxTransfers; i++) {
        checkBoxes.push(<FormControlLabel key={i} control={<Checkbox value={i} onChange={onCheckboxChange} />} label={ i > 0 ? `Пересадок: ${i}` : 'Без пересадок'} />)
    }

    return(
        <FormGroup>
            {   flights &&
                checkBoxes}
        </FormGroup>
    )
})

export default FilterTransfers;
