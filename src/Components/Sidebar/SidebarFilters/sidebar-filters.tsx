import React, { memo } from "react";
import { useSelector } from "react-redux";
import {
    FormControl,
    FormLabel,
} from "@mui/material";

import { FilterTransfers } from "./FilterTransfers";
import { FilterName } from "./FilterName";
import { IReducers } from "store/Reducers/types";
import FilterPrice from "./FilterPrice/filter-price";

const SidebarFilters = memo(() => {
    const flights = useSelector((state: IReducers) => state.flights.flights)

    return (
        <FormControl>
            <FormLabel id="filters">Фильтровать</FormLabel>
            {
                flights.length > 0 &&
                 <>
                     <FilterTransfers/>
                     <FilterPrice flights={flights}/>
                     <FilterName flights={flights}/>
                 </>
            }
        </FormControl>
    )
})

export default SidebarFilters;
