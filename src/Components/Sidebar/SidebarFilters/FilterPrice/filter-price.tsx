import React, { memo, useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    FormControl,
    FormGroup,
    Input
} from "@mui/material";

import { IFlightItem } from "Interfaces";
import { setFilter } from "Store/Actions/FlightsActions/flights-actions";
import { IFilterPrice } from "./types";
import {IReducers} from "store/Reducers/types";

const FilterPrice = memo(({flights}: IFilterPrice) => {
    const dispatch = useDispatch()

    const filters = useSelector((state: IReducers) => state.flights.filters)

    const maxPrice = useMemo( () => Math.max(...flights.map((flightItem: IFlightItem) => {
            const preparePassengerPrices = flightItem.flight.price.passengerPrices
                .filter( passengerPrice => passengerPrice.passengerType.uid === 'ADULT' );

            return parseFloat(preparePassengerPrices.map(price => price.singlePassengerTotal.amount)[0]);
        })
    ),[flights])

    useEffect( () => {
        const { minSum } = filters.price;

        const newFilters = {
            ...filters,
            ...{ price:
                    {
                        minSum: minSum,
                        maxSum: maxPrice
                    }
            }
        }

        dispatch(setFilter(newFilters))
    },[] )

    const onMinPriceChange = (event: React.FocusEvent<HTMLInputElement>) => {
        let value = parseInt(event.target.value)

        if ( isNaN( value ) ) {
            return;
        }

        const { minSum, maxSum } = filters.price;

        if ( value > maxSum ) {
            value = maxSum - 10000;
            event.target.value = value.toString()
        }

        if ( value < parseInt( event.target.min ) ) {
            value = minSum
            event.target.value = value.toString()
        }

        const newFilters = {
            ...filters,
            ...{ price:
                    {
                        minSum: value,
                        maxSum: maxSum
                    }
            }
        }

        dispatch(setFilter(newFilters))
    }

    const onMaxPriceChange = (event: React.FocusEvent<HTMLInputElement>) => {
        let value = parseInt(event.target.value)

        if ( isNaN( value ) )
            return;

        const { minSum, maxSum } = filters.price;

        if ( value < minSum ) {
            value = minSum + 10000;
            event.target.value = value.toString()
        }

        if ( value > parseInt( event.target.max ) ) {
            value = maxSum
            event.target.value = value.toString()
        }

        const newFilters = {
            ...filters,
            ...{ price:
                    {
                        minSum: minSum,
                        maxSum: value
                    }
            }
        }

        dispatch(setFilter(newFilters))
    }

    return(
        <FormGroup>
            <FormControl>
                <Input id="minPrice"
                       type="number"
                       placeholder="Минимальная цена"
                       defaultValue={0}
                       inputProps={{
                           min: '0',
                           max: `${maxPrice}`,
                       }}
                       onBlur={onMinPriceChange}
                />
            </FormControl>
            <FormControl>
                <Input id="maxPrice"
                       type="number"
                       placeholder="Максимальная цена"
                       defaultValue={maxPrice}
                       inputProps={{
                           min: '0',
                           max: `${maxPrice}`,
                       }}
                       onBlur={onMaxPriceChange}
                />
            </FormControl>
        </FormGroup>
    )
})

export default FilterPrice;
