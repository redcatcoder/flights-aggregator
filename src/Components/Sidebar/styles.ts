import IStyles from "Config/styles";

export const Styles: IStyles = {
    sidebarContainer: {
        paddingTop: 2,
        display: 'block',
    },
    sidebarItem: {
        display: 'flex',
        margin: '0 auto',
        width: '50%'
    }
}
