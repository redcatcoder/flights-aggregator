export interface ISidebarSorting {
    sorting: string,
    setSorting: (sorting: string) => void
}