import React, { memo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    FormControl,
    FormControlLabel,
    FormLabel,
    Radio,
    RadioGroup
} from "@mui/material";
import { setSorting } from "Store/Actions/FlightsActions/flights-actions";

import {IReducers} from "store/Reducers/types";

const SidebarSorting = memo(() => {
    const dispatch = useDispatch();

    const sorting = useSelector((state: IReducers) => state.flights.sorting)

    useEffect(() => {
        dispatch(setSorting('priceUp'))
    }, [])

    const onRadioClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        dispatch(setSorting(event.target.value));
    }

    return (
        <FormControl>
            <FormLabel id="sorting">Сортировать</FormLabel>
            {
                sorting &&
                <RadioGroup
                    defaultValue={sorting}
                    name="sorting-group"
                >
                    <FormControlLabel value="priceUp" control={<Radio onChange={onRadioClick}/>} label="По возрастанию цены" />
                    <FormControlLabel value="priceDown" control={<Radio onChange={onRadioClick}/>} label="По убыванию цены" />
                    <FormControlLabel value="timeInFly" control={<Radio onChange={onRadioClick}/>} label="По времени в пути" />
                </RadioGroup>
            }
        </FormControl>
    )
})

export default SidebarSorting;
