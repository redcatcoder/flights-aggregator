import React, { memo } from 'react';
import { applyMiddleware, createStore } from 'redux'
import thunk from "redux-thunk";
import { Container, Grid, ThemeProvider } from "@mui/material";
import { Provider } from "react-redux";
import { Theme } from "Config";
import rootReducer from 'Store/Reducers/root-reducer'
import { Sidebar } from "Components/Sidebar";
import { CardList } from "Components/CardList";
import { Styles } from "./styles";

export const App = memo(() => {
    const store = createStore(rootReducer, applyMiddleware(thunk))

    return (
        <Provider store={store}>
            <ThemeProvider theme={Theme}>
                <Container className="App" maxWidth={false} sx={Styles.appContainer}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} md={4}>
                            <Sidebar/>
                        </Grid>
                        <Grid item xs={12} md={8} sx={{padding: "0!important"}}>
                            <CardList/>
                        </Grid>
                    </Grid>
                </Container>
            </ThemeProvider>
        </Provider>
    );
})
