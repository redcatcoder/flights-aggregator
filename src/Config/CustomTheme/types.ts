import {PaletteOptions, ThemeOptions} from "@mui/material/styles";

interface ICustomPalette extends PaletteOptions {
    sidebar?: {
        title: string
    }
}

export interface ICustomThemeOptions extends ThemeOptions {
    palette: ICustomPalette
}
