import { createTheme } from '@mui/material/styles';
import { ICustomThemeOptions } from './types';

const CustomThemeOptions: ICustomThemeOptions = {
    palette: {
        primary: {
            light: '#00a6e1',
            main: '#0087c9',
            contrastText: '#ffffff',
        },
        secondary: {
            main: '#ffb168',
            dark: '#ff8e1d',
            contrastText: '#ffffff',
        },
        sidebar: {
            title: '#425668',
        },
        contrastThreshold: 3,
        tonalOffset: 0.2,
    },
    components: {
        MuiTypography: {
            defaultProps: {
                variantMapping: {
                    body1: 'span',
                    body2: 'p',
                },
            },
        },
    },
}

const theme = createTheme(CustomThemeOptions);

export default theme;
