import IStyles from './styles'
import Theme from './CustomTheme/theme'

const FLIGHT_URL = 'http://localhost:3000/server/flights.json'

export {
    IStyles,
    Theme,
    FLIGHT_URL
}
